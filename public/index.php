<?php

include_once __DIR__.'/../vendor/autoload.php';

spl_autoload_register(function ($class) {
    $path =  __DIR__.'/../src/'. str_replace('\\', '/', $class) . '.php';
    /** @noinspection PhpIncludeInspection */
    include_once $path;
});

$x = new \FileApi\Kernel\FileApiService(
    __DIR__ . '/../config',
    getenv('ENVIRONMENT') !== false ? getenv('ENVIRONMENT') : 'dev'
);

$x->run();
