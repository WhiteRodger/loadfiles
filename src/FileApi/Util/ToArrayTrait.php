<?php

declare(strict_types=1);

namespace FileApi\Util;

/**
 * Class ToArrayTrait
 * @package FileApi\Util
 */
trait ToArrayTrait
{
    /**
     * @return array
     */
    public function toArray() : array
    {
        $data = [];

        foreach ($this as $key => $value) {
            $data[ucfirst($key)] = $value;
        }

        return $data;
    }
}
