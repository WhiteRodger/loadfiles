<?php

declare(strict_types=1);

namespace FileApi\Util;

/**
 * Class FileServerSettings
 * @package FileApi\Util
 */
class FileServerSettings
{
    /**
     * @var string
     */
    private $baseDir;

    /**
     * FileServerSettings constructor.
     * @param string $baseDir
     */
    public function __construct(string $baseDir)
    {
        $this->baseDir = $baseDir;
    }

    /**
     * @return string
     */
    public function getBaseDir(): string
    {
        return $this->baseDir;
    }
}
