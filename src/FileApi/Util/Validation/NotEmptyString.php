<?php

declare(strict_types=1);

namespace FileApi\Util\Validation;

/**
 * Class NotEmptyString
 * @package FileApi\Util\Validation
 */
class NotEmptyString implements ValidatorInterface
{
    /**
     * @param $value
     * @param array|null $mixed
     * @return mixed
     */
    public function validate($value, array $mixed = null)
    {
        return new SimpleValidationResult(
            $value !== null && is_string($value) && strlen(trim($value)) > 0,
            'Variable must contain not empty and not zerofill string'
        );
    }
}
