<?php

declare(strict_types=1);

namespace FileApi\Util\Validation;

/**
 * Class PositiveInteger
 * @package FileApi\Util\Validation
 */
class PositiveInteger implements ValidatorInterface
{

    /**
     * @param $value
     * @param array|null $mixed
     * @return mixed
     */
    public function validate($value, array $mixed = null)
    {
        return new SimpleValidationResult(
            $value !== null && is_int($value) && $value > 0,
            'Provided value must be positive integer',
            $value
        );
    }
}
