<?php

declare(strict_types=1);

namespace FileApi\Util\Validation;

/**
 * Interface ValidatorInterface
 * @package FileApi\Util\Validation
 */
interface ValidatorInterface
{
    /**
     * @param $value
     * @param array|null $mixed
     * @return mixed
     */
    public function validate($value, array $mixed = null);
}
