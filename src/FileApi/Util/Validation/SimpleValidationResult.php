<?php

declare(strict_types=1);

namespace FileApi\Util\Validation;

use FileApi\Exception\ProtocolException\ActionException\ValidationException;

/**
 * Class SimpleValidationResult
 * @package FileApi\Util\Validation
 */
class SimpleValidationResult extends ValidationResult
{
    /**
     * Constructor
     *
     * @param boolean $booleanResult
     * @param string  $message
     * @param mixed   $variable
     */
    public function __construct($booleanResult, $message, $variable = null)
    {
        if ($booleanResult !== true) {
            parent::__construct(new ValidationException($message, $variable));
        }
    }
}
