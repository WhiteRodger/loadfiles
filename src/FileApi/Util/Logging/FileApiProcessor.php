<?php

declare(strict_types=1);

namespace FileApi\Util\Logging;

/**
 * Class FileApiProcessor
 * @package FileApi\Util\Logging
 */
class FileApiProcessor
{
    /**
     * @var string
     */
    private $session;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * BillingProcessor constructor.
     * @param string $login
     * @param string $password
     */
    public function __construct(string $login = '', string $password = '')
    {
        $this->login = $login;
        $this->password = $password;
        $this->session = $this->createSessionGuid();
    }

    /**
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        $record['extra']['session'] = $this->session;
        if ($this->login !== '') {
            $record['extra']['login'] = $this->login;
        }
        if ($this->password !== '') {
            $record['extra']['password'] = $this->password;
        }

        return $record;
    }

    /**
     * @return string
     */
    private function createSessionGuid() : string
    {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535),
            mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
}
