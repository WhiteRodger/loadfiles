<?php

declare(strict_types=1);

namespace FileApi\Util\Logging;

use Psr\Log\LoggerInterface;

/**
 * Class LoggerReferenceTrait
 * @package FileApi\Util\Logging
 */
trait LoggerReferenceTrait
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Assigns logger
     *
     * @param LoggerInterface $logger
     * @return void
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Returns logger
     *
     * @return LoggerInterface
     */
    public function getLogger() : LoggerInterface
    {
        if ($this->logger === null) {
            return DefaultLogger::getInstance();
        }

        return $this->logger;
    }
}
