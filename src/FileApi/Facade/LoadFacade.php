<?php

declare(strict_types=1);

namespace FileApi\Facade;

use FileApi\Entity\ActionProtocol\Request\LoadRequest;
use FileApi\Exception\DiException;
use FileApi\Exception\ProtocolException\ActionException\BadFileInfoException;
use FileApi\Exception\ProtocolException\ActionException\FileNotFoundException;
use FileApi\Util\FileServerSettings;

/**
 * Class LoadFacade
 * @package FileApi\Facade
 */
class LoadFacade implements LoadFacadeInterface
{
    /**
     * @var FileServerSettings
     */
    private $fileServerSettings;

    /**
     * @return FileServerSettings
     * @throws DiException
     */
    public function getFileServerSettings(): FileServerSettings
    {
        if ($this->fileServerSettings == null) {
            throw new DiException('FileServerSettings');
        }
        return $this->fileServerSettings;
    }

    /**
     * @param FileServerSettings $fileServerSettings
     */
    public function setFileServerSettings(FileServerSettings $fileServerSettings)
    {
        $this->fileServerSettings = $fileServerSettings;
    }

    /**
     * @param LoadRequest $request
     * @return string
     * @throws BadFileInfoException
     * @throws DiException
     * @throws FileNotFoundException
     */
    public function loadFile(LoadRequest $request): string
    {
        $path = $this->getFileServerSettings()->getBaseDir() . '/' . $request->getFileName();
        
        if (file_exists($path)) {
            $data = file_get_contents($path);
            
            $length = substr($data, $request->getFrom());
            if (!$length) {
                throw new BadFileInfoException('From parameter more than the file length');
            }
            
            return substr($data, $request->getFrom());
        } else {
            throw new FileNotFoundException($request->getFileName());
        }
    }
}
