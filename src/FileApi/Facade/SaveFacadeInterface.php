<?php

declare(strict_types=1);

namespace FileApi\Facade;

use FileApi\Entity\ActionProtocol\Request\SaveRequest;
use FileApi\Entity\ActionProtocol\Response\SaveResponse;

interface SaveFacadeInterface
{
    /**
     * @param SaveRequest $request
     * @return SaveResponse
     */
    public function save(SaveRequest $request): SaveResponse;
}
