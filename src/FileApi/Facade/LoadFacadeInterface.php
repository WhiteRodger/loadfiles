<?php

declare(strict_types=1);

namespace FileApi\Facade;
use FileApi\Entity\ActionProtocol\Request\LoadRequest;

/**
 * Interface LoadFacadeInterface
 * @package FileApi\Facade
 */
interface LoadFacadeInterface
{
    /**
     * @param LoadRequest $request
     * @return string
     */
    public function loadFile(LoadRequest $request): string;
}
