<?php

declare(strict_types=1);

namespace FileApi\Exception\ProtocolException\AuthException;
use FileApi\Entity\InternalProtocol\ResponseCode;
use FileApi\Exception\ProtocolException\ProtocolException;

/**
 * Class InitializeKeyErrorException
 * @package FileApi\Exception\AuthException
 */
class InitializeKeyErrorException extends ProtocolException
{
    /**
     * InitializeKeyErrorException constructor.
     */
    public function __construct(string $message)
    {
        parent::__construct($message, ResponseCode::AUTH_ERROR);
    }
}
