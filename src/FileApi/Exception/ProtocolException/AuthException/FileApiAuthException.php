<?php

declare(strict_types=1);

namespace FileApi\Exception\ProtocolException\AuthException;
use FileApi\Entity\InternalProtocol\ResponseCode;
use FileApi\Exception\ProtocolException\ProtocolException;

/**
 * Class FileApiAuthException
 * @package FileApi\Exception\AuthException
 */
class FileApiAuthException extends ProtocolException
{
    /**
     * FileApiAuthException constructor.
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message, ResponseCode::AUTH_ERROR);
    }
}
