<?php

declare(strict_types=1);

namespace FileApi\Exception\ProtocolException;

use FileApi\Entity\InternalProtocol\ResponseCode;

/**
 * Class UnknownCommandException
 * @package FileApi\Exception
 */
class UnknownCommandException extends ProtocolException
{
    /**
     * UnknownCommandException constructor.
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message, ResponseCode::UNKNOWN_COMMAND);
    }
}
