<?php

declare(strict_types=1);

namespace FileApi\Exception\ProtocolException\ActionException;

use FileApi\Entity\InternalProtocol\ResponseCode;
use FileApi\Exception\ProtocolException\ProtocolException;

class BadFileInfoException extends ProtocolException
{
    /**
     * BadFileInfoException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct($message, ResponseCode::BAD_FILE_META_INFO);
    }
}
