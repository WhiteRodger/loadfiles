<?php

declare(strict_types=1);

namespace FileApi\Exception\ProtocolException\ActionException;

use FileApi\Entity\InternalProtocol\ResponseCode;
use FileApi\Exception\ProtocolException\ProtocolException;

/**
 * Class FileNotFoundException
 * @package FileApi\Exception\ProtocolException\ActionException
 */
class FileNotFoundException extends ProtocolException
{
    /**
     * FileNotFoundException constructor.
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct(
            sprintf('File {%s} not found', $filename),
            ResponseCode::FILE_NOT_FOUND
        );
    }
}
