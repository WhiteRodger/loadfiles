<?php

declare(strict_types=1);

namespace FileApi\Exception\ProtocolException\ActionException;
use FileApi\Entity\InternalProtocol\ResponseCode;
use FileApi\Exception\ProtocolException\ProtocolException;

/**
 * Class MissingArgumentException
 * @package FileApi\Exception\ActionException
 */
class MissingArgumentException extends ProtocolException
{
    /**
     * MissingArgumentException constructor.
     * @param string $key
     */
    public function __construct(string $key)
    {
        parent::__construct(
            sprintf('Missing argument {%s}', $key),
            ResponseCode::INVALID_ARGUMENT
        );
    }
}
