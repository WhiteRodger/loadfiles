<?php

declare(strict_types=1);

namespace FileApi\Exception\ProtocolException\ActionException;
use FileApi\Entity\InternalProtocol\ResponseCode;
use FileApi\Exception\ProtocolException\ProtocolException;

/**
 * Class ValidateArgumentException
 * @package FileApi\Exception\ActionException
 */
class ValidateArgumentException extends ProtocolException
{
    /**
     * InvalidArgumentException constructor.
     * @param string $key
     */
    public function __construct(string $key)
    {
        parent::__construct(
            sprintf('Validate argument {%s} error', $key),
            ResponseCode::INVALID_ARGUMENT
        );
    }
}
