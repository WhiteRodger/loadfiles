<?php

declare(strict_types=1);

namespace FileApi\Exception\ProtocolException\ActionException;

/**
 * Class ValidationException
 * @package FileApi\Exception\ActionException
 */
class ValidationException extends \InvalidArgumentException
{
    /**
     * @var mixed|null
     */
    private $variableValue;

    /**
     * Constructor
     *
     * @param string $message
     * @param mixed $variableValue
     */
    public function __construct($message, $variableValue = null)
    {
        parent::__construct($message);
        $this->variableValue = $variableValue;
    }

    /**
     * Returns value of variable
     *
     * @return mixed|null
     */
    public function getVariableValue()
    {
        return $this->variableValue;
    }
}
