<?php

declare(strict_types=1);

namespace FileApi\Exception\ProtocolException;

/**
 * Class ProtocolException
 * @package FileApi\Exception
 */
class ProtocolException extends \Exception
{
    /**
     * ProtocolException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct($message, $code)
    {
        parent::__construct($message, $code);
    }
}
