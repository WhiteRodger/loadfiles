<?php

declare(strict_types=1);

namespace FileApi\Exception\ProtocolException;

use FileApi\Entity\InternalProtocol\ResponseCode;

class WrongFormatException extends ProtocolException
{
    /**
     * WrongFormatException constructor.
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message, ResponseCode::WRONG_FORMAT);
    }
}
