<?php

declare(strict_types=1);

namespace FileApi\Kernel;
use FileApi\Action\AbstractAction;
use FileApi\Entity\InternalProtocol\AnswerBundle;
use FileApi\Entity\InternalProtocol\ProtocolPacket;
use FileApi\Entity\InternalProtocol\RequestBundle;
use FileApi\Entity\InternalProtocol\ResponseCode;
use FileApi\Exception\DiException;
use FileApi\Exception\ProtocolException\ProtocolException;
use FileApi\Exception\ProtocolException\UnknownCommandException;
use FileApi\Kernel\Protocol\FormatInterface;
use FileApi\Kernel\Protocol\ProtocolInterface;
use FileApi\Util\Logging\FileApiProcessor;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class FileApiService
 * @package FileApi\Kernel
 */
class FileApiService extends BaseSystemService
{
    /**
     * @var FileLocator
     */
    private $actionLocator;

    /**
     * @var string
     */
    private $actionsFolder;

    /**
     * FileApiService constructor.
     * @param string $configFileFolder
     * @param string $environment
     */
    public function __construct(string $configFileFolder, string $environment)
    {
        parent::__construct($configFileFolder, $environment);

        $this->actionsFolder = $configFileFolder . '/actions/';
        $this->actionLocator = new FileLocator($this->actionsFolder);
    }
    
    /**
     * Performs execution
     *
     * @return void
     */
    protected function startSafe()
    {
        $this->loadMainConfiguration();

        $protocol = $this->getProtocol();
        $packet = $protocol->getIncomingPacket();
        $this->receiveLogger($packet->getData());

        $this->getLogger()->log('info', "Get incoming packet", [
                "packet" => (string) $packet,
                "tags" => ["api", "request_data"]
            ]
        );

        $format = $this->getFormat();
        try {
            $rows = $format->decode($packet->getData());

            $requestBundle = new RequestBundle(
                $packet->getData(),
                $packet->getSignature(),
                $rows
            );

            $this->loadCommandConfiguration($requestBundle->getCommand());

            $answerBundle = $this->startWithInternalProtocol($requestBundle);
            $answerBundle->addParam('Time', date('Y-m-d H:i:s'));
        } catch (ProtocolException $e) {
            $this->getLogger()->debug('ProtocolException: ' . $e->getMessage());
            $params = [
                'Result' => $e->getCode(),
                'Message' => $e->getMessage(),
                'Time' => date('Y-m-d H:i:s')
            ];
            $answerBundle = new AnswerBundle($params);
        } catch (\Throwable $t) {
            $this->getLogger()->debug('Throwable error: ' . $t->getMessage());
            $params = [
                'Result' => ResponseCode::UNKNOWN_ERROR,
                'Message' => "Unknown error",
                'Time' => date('Y-m-d H:i:s')
            ];
            $answerBundle = new AnswerBundle($params);
        }

        $headers = [];

        switch ($answerBundle->getContentType()) {
            case 'application/octet-stream':
                $headers[] = 'Content-Type: octet-stream';

                $responseParams = $answerBundle->getParams();
                $response = isset($responseParams['Body']) ? $responseParams['Body'] : '';
                $this->getLogger()->debug(
                    'Binary data success send',
                    ['response' => 'Length ' . strlen($response), 'tags' => ['api', 'response', 'octet-stream']]
                );
                break;
            case 'zip':
                $headers[] = 'Content-Type: zip';

                $responseString = $format->encode($answerBundle);
                $this->getLogger()->debug(
                    'encode response for zip',
                    ['response' => $responseString, 'tags' => ['api', 'response', 'zip']]
                );

                $zipped = gzcompress($responseString, 9);
                $response = base64_encode($zipped);

                break;
            default:
                $response = $format->encode($answerBundle);
                $headers[] = 'Content-Type: json';

                $this->getLogger()->debug(
                    'encode response',
                    ['response' => $response, 'tags' => ['api', 'response']]
                );
                break;
        }

        $protocol->sendResponse(new ProtocolPacket($response, '', $headers));
    }

    /**
     * @return ProtocolInterface
     * @throws DiException
     */
    private function getProtocol(): ProtocolInterface
    {
        if (!$this->getServicesContainer()->has('protocol')) {
            $this->getLogger()->critical('Protocol not provided in dependency injection', ['tags' => ['error']]);
            throw new DiException('Protocol');
        }

        $protocol = $this->getServicesContainer()->get('protocol');
        if (!($protocol instanceof ProtocolInterface)) {
            $this->getLogger()->critical('Protocol is not instance of ProtocolInterface', ['tags' => ['error']]);
            throw new \LogicException();
        }
        return $protocol;
    }

    /**
     * @param string $data
     * @throws DiException
     * @throws \LogicException
     */
    private function receiveLogger(string $data)
    {
        if (!$this->getServicesContainer()->has('logger')) {
            $this->getLogger()->critical('Logger not provided in dependency injection', ['tags' => ['error']]);
            throw new DiException('Logger');
        }

        $logger = $this->getServicesContainer()->get('logger');

        if (!($logger instanceof LoggerInterface)) {
            throw new \LogicException();
        }

        try {
            $jsonData = json_decode($data, true);
        } catch (\Throwable $e) {
            $jsonData = [];
        }

        $login = isset($jsonData['Login']) ? $jsonData['Login'] : 'Unknown';

        $processor = new FileApiProcessor($login);
        $logger->pushProcessor($processor);

        $this->setLogger($logger);
    }

    /**
     * @return FormatInterface
     *
     * @throws DiException
     * @throws \LogicException
     */
    private function getFormat(): FormatInterface
    {
        if (!$this->getServicesContainer()->has('format')) {
            $this->getLogger()->critical('Format not provided in dependency injection', ['tags' => ['error']]);
            throw new DiException('Format');
        }

        $format = $this->getServicesContainer()->get('format');
        if (!($format instanceof FormatInterface)) {
            $this->getLogger()->critical('Format is not instance of FormatInterface', ['tags' => ['error']]);
            throw new \LogicException();
        }
        return $format;
    }

    /**
     * @param string $actionName
     */
    private function loadCommandConfiguration(string $actionName)
    {
        $loader = new YamlFileLoader($this->getServicesContainer(), $this->actionLocator);
        try {
            $actionFile = lcfirst($actionName) . '.yml';
            $loader->load($actionFile);
            //$this->getLogger()->debug('Success read action file ' . $actionName);
        } catch (\Exception $e) {
            $this->getLogger()->debug('Not found action file ' . $actionName);
        }
    }

    private function startWithInternalProtocol(RequestBundle $request): AnswerBundle
    {
        $diActionKey = 'action.' . strtolower($request->getCommand());
        if (!$this->getServicesContainer()->has($diActionKey)) {
            $this->getLogger()->debug($diActionKey . ' not provided in di container');
            throw new UnknownCommandException('Command ' . $request->getCommand() . ' not found');
        }

        $action = $this->getServicesContainer()->get($diActionKey);
        if (!($action instanceof AbstractAction)) {
            $this->getLogger()->critical(
                'Wrong configuration! ' . $diActionKey . ' must be instance of AbstractAction',
                ['tags' => ['error'],'object' => $this]
            );
            throw new \LogicException('Wrong configuration! ' . $diActionKey . ' must be instance of AbstractAction');
        }

        return $action->handle($request);
    }
}
