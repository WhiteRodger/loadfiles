<?php

declare(strict_types=1);

namespace FileApi\Kernel\Protocol;

use FileApi\Entity\InternalProtocol\AnswerBundle;

/**
 * Interface FormatInterface
 * @package FileApi\Kernel\Protocol
 */
interface FormatInterface
{
    /**
     * @param string $data
     * @return array
     */
    public function decode(string $data) : array;

    /**
     * @param AnswerBundle $bundle
     * @return string
     */
    public function encode(AnswerBundle $bundle) : string;
}
