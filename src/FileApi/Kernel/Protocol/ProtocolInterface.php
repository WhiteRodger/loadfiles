<?php

declare(strict_types=1);

namespace FileApi\Kernel\Protocol;

use FileApi\Entity\InternalProtocol\ProtocolHeaders;
use FileApi\Entity\InternalProtocol\ProtocolPacket;

/**
 * Interface ProtocolInterface
 * @package FileApi\Kernel\Protocol
 */
interface ProtocolInterface
{
    /**
     * @return ProtocolPacket
     */
    public function getIncomingPacket(): ProtocolPacket;

    /**
     * @return ProtocolHeaders
     */
    public function getIncomingHeaders(): ProtocolHeaders;

    /**
     * @param ProtocolPacket $packet
     * @return void
     */
    public function sendResponse(ProtocolPacket $packet);
}
