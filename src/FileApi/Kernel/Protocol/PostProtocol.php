<?php

declare(strict_types=1);

namespace FileApi\Kernel\Protocol;

use FileApi\Entity\InternalProtocol\ProtocolHeaders;
use FileApi\Entity\InternalProtocol\ProtocolPacket;
use FileApi\Util\Logging\LoggerReferenceTrait;

/**
 * Class PostProtocol
 * @package FileApi\Kernel\Protocol
 */
class PostProtocol implements ProtocolInterface
{
    use LoggerReferenceTrait;

    /**
     * PostProtocol constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return ProtocolPacket
     */
    public function getIncomingPacket(): ProtocolPacket
    {
        $signature = isset($_SERVER['HTTP_SIGNATURE']) ? $_SERVER['HTTP_SIGNATURE'] : '';
        $signature = getenv('ENVIRONMENT') === 'dev' && isset($_GET['signature']) ? $_GET['signature'] : $signature;
        $headers = [];
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $headers['CONTENT_TYPE'] = $_SERVER['CONTENT_TYPE'];
        }

        $packet = new ProtocolPacket(
            file_get_contents("php://input"),
            $signature,
            $headers
        );

        return $packet;
    }

    /**
     * @return ProtocolHeaders
     */
    public function getIncomingHeaders(): ProtocolHeaders
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? $_SERVER["CONTENT_TYPE"] : '';
        return new ProtocolHeaders($contentType);
    }

    /**
     * @param ProtocolPacket $packet
     * @return void
     */
    public function sendResponse(ProtocolPacket $packet)
    {
        if (!in_array(['Content-Type: zip', 'Content-Type: octet-stream'], $packet->getHeaders())) {
            $this->getLogger()->log(
                'info', "Send response with packet",
                ["tags" => ["api"], "headers" => $packet->getHeaders()]
            );
        }

        header('Signature: '. $packet->getSignature());

        if (!empty($packet->getHeaders())) {
            foreach ($packet->getHeaders() as $header) {
                header($header);
            }
        }

        echo $packet->getData();
    }
}
