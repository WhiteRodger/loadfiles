<?php

declare(strict_types=1);

namespace FileApi\Kernel;

/**
 * Interface RunnableInterface
 * @package FileApi\Kernel
 */
interface RunnableInterface
{
    /**
     * @return void
     */
    public function run();
}
