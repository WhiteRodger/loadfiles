<?php

declare(strict_types=1);

namespace FileApi\Component;

use FileApi\Exception\ProtocolException\AuthException\FileApiAuthException;
use FileApi\Util\Logging\LoggerReferenceTrait;

/**
 * Class AuthComponent
 * @package FileApi\Component
 */
class AuthComponent implements AuthComponentInterface
{
    use LoggerReferenceTrait;
    
    public function auth(string $signature, string $string, string $publicKey)
    {
        $serverHmac = hash_hmac('sha256', $string, $publicKey);
        if ($serverHmac != $signature) {
            throw new FileApiAuthException('Signature failure');
        }
    }
}
