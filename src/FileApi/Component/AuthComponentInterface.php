<?php

declare(strict_types=1);

namespace FileApi\Component;

/**
 * Interface AuthComponentInterface
 * @package FileApi\Component
 */
interface AuthComponentInterface
{
    /**
     * @param string $signature
     * @param string $string
     * @param string $publicKey
     */
    public function auth(string $signature, string $string, string $publicKey);
}
