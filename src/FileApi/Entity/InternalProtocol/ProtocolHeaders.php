<?php

declare(strict_types=1);

namespace FileApi\Entity\InternalProtocol;
use FileApi\Util\ToStringTrait;

/**
 * Class ProtocolHeaders
 * @package FileApi\Entity\InternalProtocol
 */
class ProtocolHeaders
{
    use ToStringTrait;

    /**
     * @var string
     */
    private $contentType;

    /**
     * ProtocolHeaders constructor.
     * @param string $contentType
     */
    public function __construct(string $contentType)
    {
        $this->contentType = $contentType;
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }
}
