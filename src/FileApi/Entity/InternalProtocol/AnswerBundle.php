<?php

declare(strict_types=1);

namespace FileApi\Entity\InternalProtocol;

use FileApi\Util\ToStringTrait;

/**
 * Class AnswerBundle
 * @package FileApi\Entity\InternalProtocol
 */
class AnswerBundle
{
    use ToStringTrait;

    /**
     * @var array
     */
    private $params;

    /**
     * @var string
     */
    private $contentType = 'application/json';

    /**
     * AnswerBundle constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams() : array
    {
        return $this->params;
    }

    /**
     * @param $key
     * @param $value
     */
    public function addParam($key, $value)
    {
        $this->params[$key] = $value;
    }

    /**
     * @return string
     */
    public function getContentType() : string
    {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     */
    public function setContentType(string $contentType)
    {
        $this->contentType = $contentType;
    }
}
