<?php

declare(strict_types=1);

namespace FileApi\Entity\InternalProtocol;
use FileApi\Util\ToStringTrait;

/**
 * Class ProtocolPacket
 * @package System\Kernel\Protocol
 */
class ProtocolPacket
{
    use ToStringTrait;

    /**
     * @var string
     */
    private $data;

    /**
     * @var string
     */
    private $signature;

    /**
     * @var array
     */
    private $headers;

    /**
     * ProtocolPacket constructor.
     * @param string $data
     * @param string $signature
     * @param array $headers
     */
    public function __construct(string $data, string $signature, array $headers = [])
    {
        $this->data = $data;
        $this->signature = $signature;
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getData() : string
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getHeaders() : array
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getSignature(): string
    {
        return $this->signature;
    }
}
