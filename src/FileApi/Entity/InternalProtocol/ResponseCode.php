<?php

declare(strict_types=1);

namespace FileApi\Entity\InternalProtocol;

/**
 * Class ResponseCode
 * @package FileApi\Entity\InternalProtocol
 */
class ResponseCode
{
    const BAD_FILE_META_INFO = -7;
    const FILE_NOT_FOUND = -6;
    const INVALID_ARGUMENT = -5;
    const WRONG_FORMAT = -4;
    const AUTH_ERROR = -3;
    const UNKNOWN_COMMAND = -2;
    const UNKNOWN_ERROR = -1;
    const SUCCESS_ACTION = 1;
}
