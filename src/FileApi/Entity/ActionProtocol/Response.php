<?php

declare(strict_types=1);

namespace FileApi\Entity\ActionProtocol;

/**
 * Class Response
 * @package FileApi\Entity\ActionProtocol
 */
class Response
{
    /**
     * @var int
     */
    private $result;

    /**
     * Response constructor.
     * @param int $result
     */
    public function __construct(int $result)
    {
        $this->result = $result;
    }

    /**
     * @return int
     */
    public function getResult()
    {
        return $this->result;
    }
}
