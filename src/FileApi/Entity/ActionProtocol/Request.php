<?php

declare(strict_types=1);

namespace FileApi\Entity\ActionProtocol;

use FileApi\Util\ToStringTrait;

/**
 * Class Request
 * @package FileApi\Entity\ActionProtocol
 */
class Request
{
    use ToStringTrait;

    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $command;

    /**
     * Request constructor.
     * @param string $login
     * @param string $command
     */
    public function __construct(string $login, string $command)
    {
        $this->login = $login;
        $this->command = $command;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }
}
