<?php

declare(strict_types=1);

namespace FileApi\Entity\ActionProtocol\Response;

use FileApi\Entity\ActionProtocol\Response;

/**
 * Class SaveResponse
 * @package FileApi\Entity\ActionProtocol\Response
 */
class SaveResponse extends Response
{
    /**
     * @var string
     */
    private $fileName;

    /**
     * @var string
     */
    private $md5Hash;

    /**
     * @var int
     */
    private $fileSize;

    /**
     * SaveResponse constructor.
     * @param int $result
     * @param string $fileName
     * @param string $md5Hash
     * @param int $fileSize
     */
    public function __construct(int $result, string $fileName, string $md5Hash, int $fileSize)
    {
        parent::__construct($result);
        $this->fileName = $fileName;
        $this->md5Hash = $md5Hash;
        $this->fileSize = $fileSize;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getMd5Hash(): string
    {
        return $this->md5Hash;
    }

    /**
     * @return int
     */
    public function getFileSize(): int
    {
        return $this->fileSize;
    }
}
