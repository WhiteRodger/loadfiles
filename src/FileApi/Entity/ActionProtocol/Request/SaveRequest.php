<?php

declare(strict_types=1);

namespace FileApi\Entity\ActionProtocol\Request;

use FileApi\Entity\ActionProtocol\Request;

/**
 * Class SaveRequest
 * @package FileApi\Entity\ActionProtocol\Request
 */
class SaveRequest extends Request
{
    /**
     * @var string
     */
    private $file;

    /**
     * SaveRequest constructor.
     * @param string $login
     * @param string $command
     * @param string $file
     */
    public function __construct(string $login, string $command, string $file)
    {
        parent::__construct($login, $command);
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }
}
