<?php

declare(strict_types=1);

namespace FileApi\Entity\ActionProtocol\Request;

use FileApi\Entity\ActionProtocol\Request;

/**
 * Class LoadRequest
 * @package FileApi\Entity\ActionProtocol
 */
class LoadRequest extends Request
{
    /**
     * @var string
     */
    private $fileName;

    /**
     * @var int
     */
    private $from;

    /**
     * LoadRequest constructor.
     * @param string $login
     * @param string $command
     * @param string $fileName
     * @param int $from
     */
    public function __construct(string $login, string $command, string $fileName, int $from)
    {
        parent::__construct($login, $command);
        $this->fileName = $fileName;
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return int
     */
    public function getFrom(): int
    {
        return $this->from;
    }
}
