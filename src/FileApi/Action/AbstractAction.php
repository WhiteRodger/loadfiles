<?php

declare(strict_types=1);

namespace FileApi\Action;

use FileApi\Component\AuthComponentInterface;
use FileApi\Entity\ActionProtocol\Request;
use FileApi\Entity\InternalProtocol\AnswerBundle;
use FileApi\Entity\InternalProtocol\RequestBundle;
use FileApi\Exception\DiException;
use FileApi\Exception\ProtocolException\AuthException\InitializeKeyErrorException;
use FileApi\Util\Logging\LoggerReferenceTrait;

abstract class AbstractAction
{
    use LoggerReferenceTrait;

    /**
     * @var AuthComponentInterface
     */
    private $authComponent;

    /**
     * @return AuthComponentInterface
     * @throws DiException
     */
    public function getAuthComponent(): AuthComponentInterface
    {
        if ($this->authComponent === null) {
            throw new DiException('AuthComponentInterface');
        }
        return $this->authComponent;
    }

    /**
     * @param AuthComponentInterface $authComponent
     */
    public function setAuthComponent(AuthComponentInterface $authComponent)
    {
        $this->authComponent = $authComponent;
    }

    /**
     * @param Request $request
     * @param RequestBundle $bundle
     * @throws DiException
     * @throws InitializeKeyErrorException
     */
    public function auth(Request $request, RequestBundle $bundle)
    {
        $publicKey = $this->getPublicKey($request->getLogin());
        return $this->getAuthComponent()->auth($bundle->getSignature(), $bundle->getRequest(), $publicKey);
    }

    /**
     * @param string $login
     * @return string
     * @throws InitializeKeyErrorException
     */
    private function getPublicKey(string $login): string
    {
        $array = [
            'portal' => 'Pc2hoec4ol8date0',
            'billing' => 'w1es5Aatf9hevN657r'
        ];
        
        if (isset($array[$login])) {
            return $array[$login];
        } else {
            throw new InitializeKeyErrorException("Public key for " . $login . " not found!");
        }
    }

    /**
     * @param RequestBundle $requestBundle
     * @return AnswerBundle
     */
    abstract public function handle(RequestBundle $requestBundle): AnswerBundle;
}
