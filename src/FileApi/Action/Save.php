<?php

declare(strict_types=1);

namespace FileApi\Action;

use FileApi\Entity\ActionProtocol\Request\SaveRequest;
use FileApi\Entity\InternalProtocol\AnswerBundle;
use FileApi\Entity\InternalProtocol\RequestBundle;
use FileApi\Exception\DiException;
use FileApi\Facade\SaveFacadeInterface;
use FileApi\Util\Validation\ArrayReaderAdapter;
use FileApi\Util\Validation\NotEmptyString;

class Save extends AbstractAction
{
    /**
     * @var SaveFacadeInterface
     */
    private $saveFacade;

    /**
     * @return SaveFacadeInterface
     * @throws DiException
     */
    public function getSaveFacade(): SaveFacadeInterface
    {
        if ($this->saveFacade == null) {
            throw new DiException('SaveFacade');
        }
        return $this->saveFacade;
    }

    /**
     * @param SaveFacadeInterface $saveFacade
     */
    public function setSaveFacade(SaveFacadeInterface $saveFacade)
    {
        $this->saveFacade = $saveFacade;
    }
    
    /**
     * @param array $params
     * @return SaveRequest
     * @throws \FileApi\Exception\ProtocolException\ActionException\ValidateArgumentException
     */
    private function validation(array $params): SaveRequest
    {
        $reader = new ArrayReaderAdapter($params);

        return new SaveRequest(
            $reader->readWith(new NotEmptyString(), 'Login'),
            $reader->readWith(new NotEmptyString(), 'Command'),
            $reader->readWith(new NotEmptyString(), 'File')
        );
    }
    
    /**
     * @param RequestBundle $requestBundle
     * @return AnswerBundle
     */
    public function handle(RequestBundle $requestBundle): AnswerBundle
    {
        $request = $this->validation($requestBundle->getParams());
        $this->auth($request, $requestBundle);

        $response = $this->getSaveFacade()->save($request);

        $answerBundle = new AnswerBundle(
            [
                "FileName" => $response->getFileName(),
                "MD5Hash" => $response->getMd5Hash(),
                "Size" => $response->getFileSize()
            ]
        );

        return $answerBundle;
    }
}
