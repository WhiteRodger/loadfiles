<?php

declare(strict_types=1);

namespace FileApi\Action;

use FileApi\Entity\ActionProtocol\Request\LoadRequest;
use FileApi\Entity\InternalProtocol\AnswerBundle;
use FileApi\Entity\InternalProtocol\RequestBundle;
use FileApi\Exception\DiException;
use FileApi\Facade\LoadFacadeInterface;
use FileApi\Util\Validation\ArrayReaderAdapter;
use FileApi\Util\Validation\NotEmptyString;
use FileApi\Util\Validation\PositiveInteger;

/**
 * Class Load
 * @package FileApi\Action
 */
class Load extends AbstractAction
{
    /**
     * @var LoadFacadeInterface
     */
    private $loadFacade;

    /**
     * @return LoadFacadeInterface
     * @throws DiException
     */
    public function getLoadFacade(): LoadFacadeInterface
    {
        if ($this->loadFacade == null) {
            throw new DiException('LoadFacade');
        }
        return $this->loadFacade;
    }

    /**
     * @param LoadFacadeInterface $loadFacade
     */
    public function setLoadFacade(LoadFacadeInterface $loadFacade)
    {
        $this->loadFacade = $loadFacade;
    }
    
    /**
     * @param array $params
     * @return LoadRequest
     * @throws \FileApi\Exception\ProtocolException\ActionException\ValidateArgumentException
     */
    private function validation(array $params): LoadRequest
    {
        $reader = new ArrayReaderAdapter($params);
        $from = 0;
        if (isset($params['From'])) {
            $from = $reader->readWith(new PositiveInteger(), 'From');
        }
        
        return new LoadRequest(
            $reader->readWith(new NotEmptyString(), 'Login'),
            $reader->readWith(new NotEmptyString(), 'Command'),
            $reader->readWith(new NotEmptyString(), 'FileName'),
            $from
        );
    }

    
    /**
     * @param RequestBundle $requestBundle
     * @return AnswerBundle
     */
    public function handle(RequestBundle $requestBundle): AnswerBundle
    {
        $request = $this->validation($requestBundle->getParams());
        $this->auth($request, $requestBundle);
        
        $body = $this->getLoadFacade()->loadFile($request);
        
        $answerBundle = new AnswerBundle(
            [
                "Body" => $body
            ]
        );
        $answerBundle->setContentType('application/octet-stream');

        return $answerBundle;
    }
}
